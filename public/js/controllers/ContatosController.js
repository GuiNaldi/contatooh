angular.module('contatooh').controller('ContatosController',
    function ($scope, /*$http*/ $resource) {
        /* - Angular Puro
        $scope.total = 0;

        $scope.incrementa = function () {
            $scope.total++;
        }

        $scope.contatos = [
            {
                '_id': 1,
                'nome': 'Contato Angular 1',
                'email': 'cont1@empresa.com.br'
            },
            {
                '_id': 2,
                'nome': 'Contato Angular 2',
                'email': 'cont2@empresa.com.br'
            },
            {
                '_id': 3,
                'nome': 'Contato Angular 3',
                'email': 'cont3@empresa.com.br'
            }
        ];
        */

        /*$http.get('/contatos')
            .then(function(res) {
                $scope.contatos = res.data;
            },
            function(statusText) {
                console.log('Não foi possível obter a lista de contatos');
                console.log(statusText);
            });*/
        
        $scope.mensagem = {};
        var Contato = $resource('contatos/:id');
        
        var buscaContatos = function() {
        Contato.query(
            function(contatos) {
                $scope.contatos = contatos;
            },
            function(erro) {
                $scope.mensagem = {texto: "Não foi possível realizar a requisição." + erro};
            }
        )};

        buscaContatos();

        $scope.remove = function(contato) {
            Contato.delete({id : contato._id}
            ,function() {
                buscaContatos();
                $scope.mensagem = {
                    class:"info",
                    texto:"Contato excluído com sucesso!"
                };
            },
            function(erro){
                 $scope.mensagem = {
                     class:"info",
                     texto:"Erro ao excluir contato! " + erro};
            });
        }

        $scope.filtro = '';
        $scope.mensagem = {texto: ''};
    }
);