module.exports = function(app) {
   var controller = app.controllers.contato;
   app.get('/contatos', controller.listaContatos);
   app.route('/contatos/:id')
    .get(controller.obtemContato)
    .delete(controller.removeContato);
};