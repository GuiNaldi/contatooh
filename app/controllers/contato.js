var contatos = [
   {
      _id: 1,
      nome: 'Contato Exemplo 1',
      email: 'cont1@empresa.com.br'
   },
   {
      _id: 2,
      nome: 'Contato Exemplo 2',
      email: 'cont2@empresa.com.br'
   },
   {
      _id: 3,
      nome: 'Contato Exemplo 3',
      email: 'cont3@empresa.com.br'
   }
];

module.exports = function() {
   var controller = {};
   
   controller.listaContatos = function(req, res) {
      res.json(contatos);
   };

   controller.obtemContato = function(req, res) {
      //console.log(req.params.id);

      var idContato = req.params.id;
      
      var filtrados = contatos.filter(function(contato) {
         return contato._id == idContato;
      });

      // O método filter sempre retorna um vetor,
      // mesmo que haja um só resultado. Por isso,
      // se houver encontrado alguém, pegamos a primeira
      // posição (0) do vetor
      if(filtrados.length > 0) { // Encontrou algo
         var contato = filtrados[0];
         res.json(contato);
      }
      else {
         res.status(404).send('Contato ' + idContato +
            ' não encontrado.');
      }
   }

   controller.removeContato = function(req,res) {
         contatos = contatos.filter(function(contato){
               return contato._id != req.params.id;
         });
         //HTTP 204: Ok, mas não há conteúdo na resposta
         res.status(204).end();
   }

   return controller;
};